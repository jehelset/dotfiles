;; init.el --- Init File -*- lexical-binding: t -*-
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

(use-package emacs
  :ensure t
  :init
  (global-auto-revert-mode t)
  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (delete-selection-mode t)
  (defun jehelset-jump-to-minibuffer()
    "Jump to minibuffer."
    (interactive)
    (when (active-minibuffer-window)
      (select-frame-set-input-focus (window-frame (active-minibuffer-window)))
      (select-window (active-minibuffer-window))))

  (defun jehelset-edit-user-init-file ()
    "Edit user init file"
    (interactive)
    (find-file user-init-file))
  (defun jehelset-reload-user-init-file ()
    "Reload init file."
    (interactive)
    (load-file user-init-file))
  (defun jehelset-insert-timestamp ()
    (interactive)
    (insert (format-time-string "%Y-%m-%d %H:%M:%S UTC%:z")))
  (defun jehelset-toggle-window-dedicated ()
    "Control whether or not Emacs is allowed to display another
buffer in current window."
    (interactive)
    (message
     (if (let (window (get-buffer-window (current-buffer)))
	   (set-window-dedicated-p window (not (window-dedicated-p window))))
	 "%s is dedicated."
       "%s is free.")
     (current-buffer)))
  :config
  (add-to-list 'default-frame-alist
	       '(vertical-scroll-bars . nil))
  (fset 'yes-or-no-p 'y-or-n-p)
  (setq custom-file (expand-file-name "custom.el" user-emacs-directory))
  (setq-default indent-tabs-mode nil)
  (when (file-exists-p custom-file)
    (load custom-file))
  (define-key input-decode-map [?\C-\M-m] [C-M-m])
  (set-face-attribute 'default nil
		    :family "Hack"
		    :height 120
		    :weight 'normal
		    :width 'normal)
  :custom
  (fill-column 100)
  (load-prefer-newer t)
  (resize-mini-windows t)
  (confirm-kill-processes nil)
  (create-lockfiles nil)
  (user-full-name "John Eivind Helset")
  (user-mail-address "private@jehelset.no")
  (backup-directory-alist `((".*" . ,temporary-file-directory)))
  (auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))
  (scroll-margin 0)
  (scroll-conservatively 10000)
  (scroll-preserve-screen-position 1)
  (url-http-attempt-keepalives nil)
  (split-width-threshold 200)
  (display-buffer-alist
   `(("\\.gdbinit"
      (display-buffer-reuse-window display-buffer-in-side-window))))

  :bind
  (:map global-map
	("C-h h" . nil)
	("C-M-w" . nil)
	("C-x C-z" . nil)
	("C-q" . nil))

  ("H-k b" . (lambda () (interactive) (kill-line 0)))
  ("M-Z" . zap-up-to-char)
  ("H-i i" . jehelset-edit-user-init-file)
  ("H-i l" . jehelset-reload-user-init-file)

  ("H-b p" . previous-buffer)
  ("H-b n" . next-buffer)
  ("H-b q" . kill-this-buffer)

  ("H-C-n" . scroll-up-command)
  ("H-C-b" . beginning-of-line)
  ("H-C-p" . scroll-down-command)
  ("H-C-f" . end-of-line)

  ("H-j f" . end-of-line)
  ("H-j p" . beginning-of-buffer)
  ("H-j m" . jehelset-jump-to-minibuffer)
  ("H-j n" . end-of-buffer)
  ("H-j b" . beginning-of-line)
  ("H-f p" . fill-paragraph)
  ("H-f w" . whitespace-cleanup)
  ("H-f r" . fill-region)

  ("H-w r" . split-window-right)
  ("H-w b" . split-window-below)
  ("H-M-q" . delete-window)
  ("H-M-o" . other-window)
  ("H-w f" . make-frame-command)
  ("H-w d" . jehelset-toggle-window-dedicated)

  ("H-u p m" . (lambda()(interactive)(insert "⁻")))
  ("H-u p 0" . (lambda()(interactive)(insert "⁰")))
  ("H-u p 1" . (lambda()(interactive)(insert "¹")))
  ("H-u p 2" . (lambda()(interactive)(insert "²")))
  ("H-u p 3" . (lambda()(interactive)(insert "³")))
  ("H-u p 4" . (lambda()(interactive)(insert "⁴")))
  ("H-u p 5" . (lambda()(interactive)(insert "⁵")))
  ("H-u p 6" . (lambda()(interactive)(insert "⁶")))
  ("H-u p 7" . (lambda()(interactive)(insert "⁷")))
  ("H-u p 8" . (lambda()(interactive)(insert "⁸")))
  ("H-u p 9" . (lambda()(interactive)(insert "⁹")))
  ("H-u p i" . (lambda()(interactive)(insert "ⁱ")))
  ("H-u p n" . (lambda()(interactive)(insert "ⁿ")))
  ("H-u P 0" . (lambda()(interactive)(insert "₀")))
  ("H-u P 1" . (lambda()(interactive)(insert "₁")))
  ("H-u P 2" . (lambda()(interactive)(insert "₂")))
  ("H-u P 3" . (lambda()(interactive)(insert "₃")))
  ("H-u P 4" . (lambda()(interactive)(insert "₄")))
  ("H-u P 5" . (lambda()(interactive)(insert "₅")))
  ("H-u P 6" . (lambda()(interactive)(insert "₆")))
  ("H-u P 7" . (lambda()(interactive)(insert "₇")))
  ("H-u P 8" . (lambda()(interactive)(insert "₈")))
  ("H-u P 9" . (lambda()(interactive)(insert "₉")))
  ("H-u P i" . (lambda()(interactive)(insert "ᵢ")))
  ("H-u P j" . (lambda()(interactive)(insert "ⱼ")))
  ("H-u P x" . (lambda()(interactive)(insert "ₓ")))
  ("H-u P +" . (lambda()(interactive)(insert "₊")))
  ("H-u o l e" . (lambda()(interactive)(insert "≤")))
  ("H-u o d" . (lambda()(interactive)(insert "≔")))
  ("H-u o e" . (lambda()(interactive)(insert "=")))
  ("H-u o E" . (lambda()(interactive)(insert "≠")))
  ("H-u o m" . (lambda()(interactive)(insert "·")))
  ("H-u o p" . (lambda()(interactive)(insert "∏")))
  ("H-u o s" . (lambda()(interactive)(insert "∑")))
  ("H-u o c" . (lambda()(interactive)(insert "×")))
  ("H-u o p" . (lambda()(interactive)(insert "∘")))
  ("H-u s m" . (lambda()(interactive)(insert "∖")))
  ("H-u s e" . (lambda()(interactive)(insert "∅")))
  ("H-u s a" . (lambda()(interactive)(insert "∀")))
  ("H-u s i" . (lambda()(interactive)(insert "∈")))
  ("H-u s I" . (lambda()(interactive)(insert "∉")))
  ("H-u s x" . (lambda()(interactive)(insert "∃")))
  ("H-u s X" . (lambda()(interactive)(insert "∄")))
  ("H-u l a" . (lambda()(interactive)(insert "∧")))
  ("H-u l o" . (lambda()(interactive)(insert "∨")))
  ("H-u l t" . (lambda()(interactive)(insert "⟙")))
  ("H-u l f" . (lambda()(interactive)(insert "⟘")))
  ("H-u l a" . (lambda()(interactive)(insert "→")))
  ("H-u g a" . (lambda()(interactive)(insert "α")))
  ("H-u g b" . (lambda()(interactive)(insert "β")))
  ("H-u g d" . (lambda()(interactive)(insert "δ")))
  ("H-u g n" . (lambda()(interactive)(insert "ν")))
  ("H-u g N" . (lambda()(interactive)(insert "Ν")))
  ("H-u g D" . (lambda()(interactive)(insert "Δ")))
  ("H-u g l" . (lambda()(interactive)(insert "λ")))
  ("H-u g L" . (lambda()(interactive)(insert "Λ")))
  ("H-u g q" . (lambda()(interactive)(insert "φ")))
  ("H-u g Q" . (lambda()(interactive)(insert "Φ")))
  ("H-u g p" . (lambda()(interactive)(insert "π")))
  ("H-u g P" . (lambda()(interactive)(insert "Π")))
  ("H-u g u" . (lambda()(interactive)(insert "υ")))
  ("H-u g U" . (lambda()(interactive)(insert "Υ")))
  ("H-u g s" . (lambda()(interactive)(insert "ψ")))
  ("H-u g S" . (lambda()(interactive)(insert "Ψ")))
  ("H-u g o" . (lambda()(interactive)(insert "ω")))
  ("H-u g O" . (lambda()(interactive)(insert "Ω")))
  ("H-u g v" . (lambda()(interactive)(insert "θ")))
  ("H-u g V" . (lambda()(interactive)(insert "Θ")))
  ("H-u g t" . (lambda()(interactive)(insert "τ")))
  ("H-u g T" . (lambda()(interactive)(insert "Τ")))
  ("H-u g e" . (lambda()(interactive)(insert "ɛ")))
  ("H-u g E" . (lambda()(interactive)(insert "Ε")))
  ("H-u g k" . (lambda()(interactive)(insert "κ")))
  ("H-u g K" . (lambda()(interactive)(insert "Κ")))
  ("H-u g r" . (lambda()(interactive)(insert "ρ")))
  ("H-u g R" . (lambda()(interactive)(insert "Ρ")))
  ("H-u g i" . (lambda()(interactive)(insert "ι")))
  ("H-u g I" . (lambda()(interactive)(insert "Ι")))
  ("H-u g g" . (lambda()(interactive)(insert "γ")))
  ("H-u g G" . (lambda()(interactive)(insert "Γ")))
  ("H-u g x" . (lambda()(interactive)(insert "ξ")))
  ("H-u g X" . (lambda()(interactive)(insert "Ξ")))
  ("H-u g 0" . (lambda()(interactive)(insert "Ϙ")))

  (:map help-mode-map
	("p" . help-go-back)
	("n" . help-go-forward))
  :hook ((prog-mode . display-line-numbers-mode)))

(use-package table
  :demand
  :bind
  ("H-t r t" . table-recognize-table)
  ("H-t r c" . table-recognize-cell)
  ("H-t r r" . table-recognize-region)
  ("H-t i t" . table-insert)
  ("H-t i c" . table-insert-column)
  ("H-t i r" . table-insert-row)
  ("H-t s v" . table-split-cell-horizontally)
  ("H-t s h" . table-split-cell-vertically)
  ("H-t d c" . table-delete-column)
  ("H-t d r" . table-delete-row)
  ("H-t >" . table-widen-cell)
  ("H-t <" . table-narrow-cell)
  ("H-t u t" . table-unrecognize-table)
  ("H-t u c" . table-unrecognize-cell)
  ("H-t u r" . table-unrecognize-region))

(use-package recentf
  :demand
  :custom
  (recentf-mode 1))

(use-package epa-file
  :demand
  :custom
  (epa-pinentry-mode 'loopback)
  (epa-file-select-keys nil))

(use-package register
  :demand
  :bind
  ("H-M-i" . insert-register))

(use-package rainbow-mode
  :ensure t)

(use-package replace
  :demand
  :bind
  ("H-r o" . occur)
  ("H-r O" . multi-occur)
  ("H-r r" . query-replace)
  ("H-r x" . query-replace-regexp)
  (:map occur-mode-map
	("n" . occur-next)
	("p" . occur-prev)
	("C-n" . jehelset-occur-display-next)
	("C-p" . jehelset-occur-display-prev))
  (:map query-replace-map
	("C-h" . nil)
	("p" . backup)
	("y" . act)
	("j" . act-and-exit)
	("Y" . automatic))
  :config
  (define-advice occur
      (:after (oldfun &rest args))
    "Something"
    (jehelset-switch-to-occur)
    (next-line))
  (defun jehelset-switch-to-occur()
    (switch-to-buffer-other-window "*Occur*"))
  (defun jehelset-occur-display-next()
    "Next."
    (interactive)
    (occur-next)
    (occur-mode-display-occurrence))
  (defun jehelset-occur-display-prev()
    "Prev."
    (interactive)
    (occur-prev)
    (occur-mode-display-occurrence)))

(use-package shackle
  :ensure t
  :custom
  (shackle-rules '((compilation-mode :noselect t)))
  (shackle-default-rule '(:select t)))

(use-package windmove
  :bind
  ("H-M-w" . windmove-up)
  ("H-M-a" . windmove-left)
  ("H-M-s" . windmove-down)
  ("H-M-d" . windmove-right))

(defun jehelset-xml-encode-region ()
  "Xml encode region."
  (interactive)
  (if (region-active-p)
      (let ((escaped-string
	     (xml-escape-string
	      (buffer-substring-no-properties
	       (region-beginning)
	       (region-end)))))
	(delete-region (region-beginning)
		       (region-end))
	(insert escaped-string))))

(use-package xml
  :defer t
  :bind (("H-x e r" . jehelset-xml-encode-region)))

(use-package whole-line-or-region
  :ensure t
  :config
  (whole-line-or-region-global-mode))

(use-package avy
  :ensure t
  :custom
  (avy-background t)
  :config
  (defun jehelset-goto-line()
    "Pick single item in live buffer"
    (interactive)
    (let ((avy-all-windows nil))
      (avy-goto-line)))
  :bind (("H-M-j" . avy-goto-word-or-subword-1)
	 ("H-M-J" . avy-goto-char)
	 ("H-j c" . avy-goto-char)
	 ("H-g" . avy-goto-line)
	 ("M-H-g" . goto-line)
	 ("H-j l" . jehelset-goto-line)))

(use-package smerge-mode
  :demand
  :config
  :bind
  ("H-s n" . smerge-next)
  ("H-s p" . smerge-prev)
  ("H-s u" . smerge-keep-upper)
  ("H-s l" . smerge-keep-lower)
  ("H-s a" . smerge-keep-all))

(use-package crux
  :ensure t
  :bind (("C-c o" . crux-open-with)))

(use-package vc
  :custom
  (vc-handled-backends nil))

(use-package electric-indent
  :bind (("<C-return>" . electric-indent-just-newline)))

(use-package vterm
  :ensure t
  :custom
  (vterm-max-scrollback 10000)
  :bind (("H-M-v" . vterm)))

(use-package paren
  :ensure t
  :hook ((prog-mode . show-paren-mode)))

(use-package transpose-frame
  :ensure t
  :bind
  ("H-w t" . transpose-frame)
  ("H-w o" . rotate-frame))


(use-package winner
  :demand
  :config
  (winner-mode 1)
  :bind(("H-M-n" . winner-redo)
	("H-M-p" . winner-undo)))

(use-package mood-line
  :ensure t
  :config (mood-line-mode 1))

(use-package modus-themes
  :ensure t
  :config (load-theme 'modus-vivendi t))

(use-package sort
  :ensure t
  :bind (("H-f s" . sort-lines)))

(use-package align
  :ensure t
  :bind (("H-f a" . align-regexp)))

(use-package yasnippet
  :ensure t
  :bind (("H-y r" . yas-reload-all)
	 ("H-y i" . yas-insert-snippet)
	 ("H-y w" . yas-new-snippet))
  :config
  (yas-global-mode))

(use-package dired
  :defer t
  :custom
  (dired-listing-switches '"-lah")
  (dired-dwim-target t)
  (dired-recursive-deletes 'always)
  (dired-recursive-copies 'always))

(use-package minibuffer
  :demand
  :custom
  (enable-recursive-minibuffers t)
  :config
  (minibuffer-depth-indicate-mode)
  :bind
  (:map minibuffer-local-completion-map
	("TAB" . nil)
	("SPC" . self-insert-command)
	("?" . self-insert-command)))

(use-package adoc-mode
  :ensure t)

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :custom (markdown-command "markdown"))

(use-package web-mode
  :ensure t
  :config)

(use-package python
  :ensure t
  :custom
  (python-shell-interpreter "ipython")
  (python-shell-interpreter-args "--simple-prompt -i")
  (python-indent-offset 4))

(use-package python-black
  :ensure t
  :after python
  :bind
  (:map python-mode-map
	("H-c f" . python-black-region)
	("H-c F" . python-black-buffer)))

(use-package geiser
  :ensure t)

(use-package compile
  :init
  (global-unset-key (kbd "M-m"))
  (push '("\\*compilation\\*" . (nil (reusable-frames . t))) display-buffer-alist)
  (push '("\\*tex-shell\\*" . (nil (reusable-frames . t))) display-buffer-alist)
  (defun jehelset-colorize-compilation-buffer ()
    (read-only-mode)
    (ansi-color-apply-on-region compilation-filter-start (point))
    (read-only-mode))
  (defun jehelset-compilation-next-error ()
    (interactive)
    (let ((display-buffer-overriding-action '(display-buffer-same-window (inhibit-same-window . nil))))
      (next-error)))
  (defun jehelset-compilation-previous-error ()
    (interactive)
    (let ((display-buffer-overriding-action '(display-buffer-same-window (inhibit-same-window . nil))))
      (previous-error)))

  (defun jehelset-switch-to-compilation-buffer()
    (interactive)
    (switch-to-buffer "*compilation*"))
  :hook
  (compilation-filter . jehelset-colorize-compilation-buffer)
  :custom
  (compilation-always-kill t)
  (compilation-ask-about-save nil)
  :bind (("H-m b" . jehelset-switch-to-compilation-buffer)
	 ("H-m n" . jehelset-compilation-next-error)
	 ("H-m p" . jehelset-compilation-previous-error)
	 ("H-m q" . kill-compilation)))

(use-package modern-cpp-font-lock
  :ensure t)

(use-package cc-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.ipp\\'" . c++-mode))
  :custom
  (blink-matching-paren nil))

(use-package haskell-mode
  :ensure t)

(use-package d-mode
  :ensure t)

(use-package rust-mode
  :ensure t)

(use-package tuareg
  :ensure t)

(use-package clang-format
  :ensure t
  :init
  (defun clang-format-save-hook-for-this-buffer ()
    "Create a buffer local save hook."
    (add-hook 'before-save-hook
	      (lambda ()
		(progn
		  (when (locate-dominating-file "." ".clang-format")
		    (clang-format-buffer))
		  ;; Continue to save.
		  nil))
	      nil
	      ;; Buffer local hook.
	      t))
  :bind
  (:map c++-mode-map
	("H-c f" . clang-format-region)
	("H-c F" . clang-format-buffer)))

(use-package cmake-mode
  :ensure t)

(use-package make-mode
  :ensure t)

(use-package company
  :ensure t
  :defer t
  :custom
  (company-tooltip-offset-display 'scrollbar)
  (company-tooltip-align-annotations t)
  (company-minimum-prefix-length 3)
  :hook ((text-mode . company-mode)
         (python-mode . company-mode)
         (cpp-mode . company-mode))
  :bind
  ("M-\\" . dabbrev-expand)
  ("H-c c" . company-complete)
  (:map company-active-map
	("C-n" . company-select-next)
	("C-p" . company-select-previous))
  (:map read-expression-map
	([tab] . company-complete)))
(use-package company-box
  :ensure t
  :hook (company-mode . company-box-mode))

(use-package ivy
  :ensure t
  :config
  (ivy-mode))

(use-package ivy-xref
  :ensure t
  :init
  (setq xref-show-definitions-function #'ivy-xref-show-defs)
  (setq xref-show-xrefs-function #'ivy-xref-show-xrefs))

(use-package swiper
  :ensure t
  :bind
  ("C-s" . swiper-isearch)
  ("C-r" . swiper-isearch-backward))

(use-package counsel
  :ensure t
  :demand
  :config
  (counsel-mode)
  :bind
  ("C-M-SPC" . counsel-mark-ring)
  ("H-U" . counsel-unicode-char)
  ("H-j i" . counsel-imenu)
  ("H-M-i" . counsel-register))

(use-package counsel-notmuch
  :ensure t)

(use-package magit
  :ensure t
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  :config
  (transient-append-suffix 'magit-push '(0 -1)
    '("+c" "Create merge-request"
      "--push-option=merge_request.create"))
  (transient-append-suffix 'magit-push '(0 -1)
    '("+b" "Target branch"
      "--push-option=merge_request.target="
      :history-key magit-revision-history
      :reader (lambda (prompt initial-input history)
		;; You could get fancier here.
		(magit-completing-read prompt nil nil nil
                                       (or initial-input "master")))))
  (transient-append-suffix 'magit-push '(0 -1)
    '("+m" "Merge on success"
      "--push-option=merge_request.merge_when_pipeline_succeeds"))
  (magit-add-section-hook 'magit-status-sections-hook
			  'magit-insert-branch-description
			  nil t)
  (magit-add-section-hook 'magit-refs-sections-hook
			  'magit-insert-branch-description))

(use-package forge
  :ensure t
  :after magit)

;; (use-package tspew
;;   :ensure nil
;;   :demand t
;;   :load-path "/home/jeh/code/github/jefftrull/tspew"
;;   :config
;;   (add-hook 'compilation-mode-hook 'tspew-mode))
  
(use-package eldev
  :ensure t)

(use-package projectile
  :ensure nil
  :demand t
  :load-path "/home/jeh/code/github/jehelset/projectile"
  :custom
  (projectile-enable-cmake-presets 1)
  :init
  (defun jehelset-clear-python (arg)
    "Clear python"
    (interactive "P")
    (with-current-buffer (python-shell-get-buffer)
      (comint-clear-buffer)))
  (defun jehelset-open-python (arg)
    "Open python"
    (interactive "P")
    (switch-to-buffer (python-shell-get-buffer)))
  (defun jehelset-run-python (arg)
    "Run python"
    (interactive "P")
    (let ((default-directory (projectile-project-root))
	  (cmd nil)
	  (dedicated nil)
	  (show nil))
      (let ((buffer
	     (python-shell-make-comint
	      (or cmd (python-shell-calculate-command))
	      (python-shell-get-process-name dedicated) show)))
	(get-buffer-process buffer))))
  (defun jehelset-kill-python (arg)
    "Kill python"
    (interactive "P")
    (delete-process (python-shell-get-process-or-error))
    (sleep-for 0.05))
  (defun jehelset-restart-python (arg)
    "Restart python"
    (interactive "P")
    (jehelset-kill-python arg)
    (jehelset-run-python arg)
    (jehelset-clear-python arg))
  (defun jehelset-dired-exec-python (arg)
    "Execute python file"
    (interactive "P")
    (mapc 'python-shell-send-file
	  (dired-get-marked-files)))
  (defun jehelset-run-ctest (arg)
    "Run counsel-test-ctest in default projectile-dir"
    (interactive "P")
    (let ((counsel-test-dir (projectile-compilation-dir)))
      (counsel-test-ctest arg)))

  (defun jehelset-python-extension-filter (names)
    "Filter names without python extension."
    (cl-remove-if-not (apply-partially #'string-match-p ".*\\.py$")
		      names))

  (defun jehelset-python-buffer-to-send ()
    "Read the name of a buffer to switch to, prompting with PROMPT."
    (projectile-completing-read
     "Send python buffer"
     (jehelset-python-extension-filter (projectile-project-buffer-names))))

  (defun jehelset-python-file-to-send ()
    "Read the name of a buffer to switch to, prompting with PROMPT."
    (projectile-completing-read
     "Send python file"
     (jehelset-python-extension-filter (projectile-current-project-files))))

  (defun jehelset-python-shell-send-project-file ()
    "Send python project file to python shell"
    (interactive)
    (let ((file-to-send (jehelset-python-file-to-send)))
      (python-shell-send-file file-to-send)))
  (defun jehelset-python-shell-send-project-buffer ()
    "Send python project buffer to python shell"
    (interactive)
    (let ((buffer-to-send (jehelset-python-buffer-to-send)))
      (with-current-buffer buffer-to-send
	(python-shell-send-buffer))))

  (define-skeleton jehelset-projectile-skel-gdbinit
    "Insert a .gdbinit template."
    nil
    ""
    resume:
    "")

  (defun jehelset-projectile-edit-gdbinit ()
    "Edit or create a .gdbinit file of the project."
    (interactive)
    (let ((file (expand-file-name ".gdbinit" (projectile-acquire-root))))
      (find-file file)
      (when (not (file-exists-p file))
	(unwind-protect
	    (jehelset-projectile-skel-gdbinit)
	  (save-buffer)))))

  (defun jehelset-projectile-open-file ()
    "Open a file of in the project"
    (interactive)
    (let* ((project-root (projectile-acquire-root))
	   (file (projectile-completing-read "Open file: "
					     (projectile-project-files project-root))))
      (when file
	(async-shell-command (format "xdg-open \"%s\"" (expand-file-name file project-root))))))

  :bind-keymap
  ("H-p" . projectile-command-map)
  :bind
  ("H-j o" . projectile-find-other-file)
  (:map projectile-command-map
	("r"   . project-query-replace-regexp)
	("H-c"  . projectile-reconfigure-project)
	("w"   . jehelset-run-ctest)
	("y y" . jehelset-run-python)
	("y c" . jehelset-clear-python)
	("y o" . jehelset-open-python)
	("y k" . jehelset-kill-python)
	("y r" . jehelset-restart-python)
	("y e" . jehelset-dired-exec-python)
	("y f" . jehelset-python-shell-send-project-file)
	("y b" . jehelset-python-shell-send-project-buffer)
	("] e" . jehelset-projectile-edit-gdbinit)
	("x o" . jehelset-projectile-open-file)
	("[ [" . forge-pull)
	("[ c p" . forge-create-pullreq)
	("[ c P" . forge-create-pullreq-from-issue)
	("[ c i" . forge-create-issue))
  :config
  (projectile-mode +1))

(use-package projectile-vterm
  :ensure nil
  :demand t
  :load-path "/home/jeh/code/gitlab/jehelset/projectile-vterm"
  :bind
  (:map projectile-command-map
	("' '" . projectile-vterm-run-vterm)
	("' \"" . projectile-vterm-run-vterm-other-window)
	("' b" . projectile-vterm-start-vterm)
	("' q" . projectile-vterm-kill-vterm)
	("' Q" . projectile-vterm-restart-vterm)
	("' l" . projectile-vterm-send-line)
	("' e" . vterm-copy-mode)
	("' r" . projectile-vterm-send-region)
	("' c" . projectile-vterm-send-buffer)))

(defun jehelset-get-authinfo-password (&rest params)
  (require 'auth-source)
  (let ((match (car (apply 'auth-source-search params))))
    (if match
	(let ((secret (plist-get match :secret)))
	  (if (functionp secret)
	      (funcall secret)
	    secret))
      (error "Password not found for %S" params))))

(use-package smime
  :demand t
  :custom
  (smime-certificate-directory (expand-file-name "mail/certs" user-emacs-directory)))

(use-package mm-encode
  :demand t
  :custom
  (mm-encrypt-option 'guided)
  (mm-sign-option 'guided))

(use-package mml-sec
  :demand t
  :custom
  (mml-secure-openpgp-encrypt-to-self t)
  (mml-secure-openpgp-sign-with-sender t)
  (mml-secure-smime-encrypt-to-self t)
  (mml-secure-smime-sign-with-sender t))

(use-package message
  :demand t
  :custom
  (message-directory (expand-file-name "mail" user-emacs-directory))
  (message-config-send nil)
  (message-kill-buffer-on-exit t)
  (message-signature "John Eivind Helset\n")
  (message-send-mail-function #'message-send-mail-with-sendmail))

(use-package sendmail
  :demand
  :custom
  (mail-signature "John Eivind Helset\n")
  (sendmail-program "msmtp")
  (mail-specify-envelope-from t)
  (message-envelope-from 'header)
  (send-mail-function #'message-send-mail-with-sendmail))

(use-package notmuch
  :defer t
  :init
  (setq notmuch-read-tags '("-unread"))
  :config
  (defun jehelset-mbsync ()
    "Run mbsync"
    (interactive)
    (shell-command "systemctl --user start mbsync.service"))
  (setq notmuch-deleted-tags '("+deleted" "-inbox" "-unread"))

  (defun jehelset-notmuch-show-view-as-patch ()
    "View the the current message as a patch."
    (interactive)
    (let* ((id (notmuch-show-get-message-id))
	   (msg (notmuch-show-get-message-properties))
	   (part (notmuch-show-get-part-properties))
	   (subject (concat "Subject: " (notmuch-show-get-subject) "\n"))
	   (diff-default-read-only t)
	   (buf (get-buffer-create (concat "*notmuch-patch-" id "*")))
	   (map (make-sparse-keymap)))
      (define-key map "q" 'notmuch-bury-or-kill-this-buffer)
      (switch-to-buffer buf)
      (let ((inhibit-read-only t))
	(erase-buffer)
	(insert subject)
	(insert (notmuch-get-bodypart-text msg part nil)))
      (set-buffer-modified-p nil)
      (diff-mode)
      (lexical-let ((new-ro-bind (cons 'buffer-read-only map)))
	(add-to-list 'minor-mode-overriding-map-alist new-ro-bind))
    (goto-char (point-min))))
  :custom
  (notmuch-search-oldest-first nil)
  (notmuch-show-empty-saved-searches t)
  (notmuch-archive-tags '("-inbox" "+archived"))
  (notmuch-saved-searches
   `((:name "unread"
	    :query "tag:unread"
	    :sort-order "newest-first"
	    :key ,(kbd "j"))
     (:name "private"
	    :query "tag:private"
	    :sort-order "newest-first"
	    :key ,(kbd "r"))
     (:name "sent"
	    :query "tag:sent"
	    :sort-order "newest-first"
	    :key ,(kbd "s"))
     (:name "gmail"
	    :query "tag:gmail"
	    :sort-order "newest-first"
	    :key ,(kbd "g"))
     (:name "ntnu"
	    :query "tag:ntnu"
	    :sort-order "newest-first"
	    :key ,(kbd "n"))
     (:name "gitlab"
	    :query "tag:gitlab"
	    :sort-order "newest-first"
	    :key ,(kbd "z"))
     (:name "projectile"
	    :query "tag:projectile"
	    :sort-order "newest-first"
	    :key ,(kbd "p"))
     (:name "flagged"
	    :query "tag:flagged"
	    :sort-order "newest-first"
	    :key ,(kbd "f"))
     (:name "emacs"
	    :query "tag:emacs"
	    :sort-order "newest-first"
	    :key ,(kbd "e"))
     (:name "cpp"
	    :query "tag:cpp"
	    :sort-order "newest-first"
	    :key ,(kbd "c"))))
  (notmuch-tagging-keys
   `((,(kbd "r") notmuch-read-tags "Read")
     (,(kbd "a") notmuch-archive-tags "Archived")
     (,(kbd "d") notmuch-deleted-tags "Delete")))
  (notmuch-tag-formats
   '(("unread" (propertize tag 'face 'notmuch-tag-unread))))
  (notmuch-hello-sections '(notmuch-hello-insert-saved-searches
			    notmuch-hello-insert-alltags))
  :bind
  ("H-o o" . notmuch)
  ("H-o f" . jehelset-mbsync)
  (:map notmuch-show-part-map
	(("d" .  jehelset-notmuch-show-view-as-patch))))

(use-package editorconfig
  :ensure t)

(use-package fish-mode
  :ensure t)

(use-package rg
  :ensure t)

(use-package eglot
  :ensure t
  :config

  (add-to-list 'eglot-server-programs 
               '(haskell-mode . ("haskell-language-server-wrapper" "--lsp")))
  (add-to-list 'eglot-server-programs
	       '((c-mode c++-mode)
		 . ("clangd"
		       "-j=8"
		       "--log=error"
		       "--malloc-trim"
		       "--background-index"
		       "--clang-tidy"
		       "--cross-file-rename"
		       "--completion-style=detailed"
		       "--pch-storage=memory"
		       "--header-insertion=never"
		       "--header-insertion-decorators=0")))
  (add-to-list 'eglot-server-programs
	    '(rust-mode
		 . ("rust-analyzer")))
  (add-to-list 'eglot-server-programs
	    '(python-mode
		 . ("pylsp")))
  :custom
  (eglot-ignored-server-capabilities '(:hoverProvider :codeActionProvider :foldingRangeProvider :inlayHintProvider))
  :bind
  (:map eglot-mode-map
	("H-c f" . eglot-format)
	("H-c F" . eglot-format-buffer)))

(put 'narrow-to-region 'disabled nil)
(put 'overwrite-mode 'disabled nil)
