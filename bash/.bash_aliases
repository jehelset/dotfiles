#
# ~/.bash_aliases
#

alias ls='ls --color=auto'
alias vt='emacs -f vterm'
alias ustow='stow -t $HOME'
alias usystemctl='systemctl --user'
